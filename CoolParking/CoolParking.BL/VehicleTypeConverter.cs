﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;

namespace CoolParking.BL
{
    class VehicleTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(VehicleType));
        }

        public override object ReadJson(JsonReader reader, Type objectType,
                                        object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.String)
            {
                if ((string)reader.Value == string.Empty)
                {
                    return -1;
                }
            }
            else if (reader.TokenType == JsonToken.Integer)
            {
                switch (Convert.ToInt32(reader.Value))
                {
                    case 0: return VehicleType.PassengerCar; 
                    case 1: return VehicleType.Truck; 
                    case 2: return VehicleType.Bus; 
                    case 3: return VehicleType.Motorcycle;

                }
            }
            return -1;
        }

        public override void WriteJson(JsonWriter writer, object value,
                                       JsonSerializer serializer)
        {
            switch (value)
            {
                case VehicleType.PassengerCar: writer.WriteValue(0); break;
                case VehicleType.Truck: writer.WriteValue(1); break;
                case VehicleType.Bus: writer.WriteValue(2); break;
                case VehicleType.Motorcycle: writer.WriteValue(3); break;
            }
        }
    }
}
