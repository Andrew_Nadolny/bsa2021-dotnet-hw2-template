﻿using System;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using System.Text;


namespace CoolParking.BL.ConsoleInterface
{
    class Program
    {
        public static string Url = "https://localhost:44398";
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("1. Display the current balance of the Parking.");
                Console.WriteLine("2. Display the amount of money earned for the current period (before writing to the log).");
                Console.WriteLine("3. Display the number of free / occupied parking places.");
                Console.WriteLine("4. Display all transactions in Parking for the current period (before logging).");
                Console.WriteLine("5. Print the transaction history (by reading data from the Transactions.log file).");
                Console.WriteLine("6. Display the list vehicle funds located in the Parking lot.");
                Console.WriteLine("7. Put the vehicle on the Parking.");
                Console.WriteLine("8. Pick up a vehicle from the Parking.");
                Console.WriteLine("9. Top up the balance of a vehicle.");
                Console.WriteLine("\n\nEnter the menu item number: ");
                int selectedItem = 0;
                if (int.TryParse(Console.ReadLine(), out selectedItem))
                {
                    switch (selectedItem)
                    {
                        case 1:
                            Console.WriteLine(String.Format("Current parking balance: {0}.", JsonConvert.DeserializeObject<decimal>(GetContent(HttpMethod.Get, String.Format("{0}/api/parking/balance", Url)))));
                            break;
                        case 2:
                            Console.WriteLine(String.Format("The amount of money earned for the current period: {0}.", GetMoneyEarnedForTheCurrentPeriod()));
                            break;
                        case 3:
                            Console.WriteLine(String.Format("Number of free places: {0}. Number of occupied places: {1}.", GetContent(HttpMethod.Get, String.Format("{0}/api/parking/freePlaces", Url)), GetOccupiedPlaces()));
                            break;
                        case 4:
                            Console.WriteLine("Parking transactions for the current period: ");
                            GetTransactionForTheCurrentPeriod();
                            break;
                        case 5:
                            ReadFromLog();
                            break;
                        case 6:
                            Console.WriteLine("List of vehicles in the Parking: ");
                            GetParkedVehicles();
                            break;
                        case 7:
                            SetVehicleOnParking();
                            break;
                        case 8:
                            RemoveVehicleFromParking();
                            break;
                        case 9:
                            TopUpVehicleBalance();
                            break;
                        default:
                            Console.WriteLine("The entered value does not correspond to any menu item.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("The entered value is not a valid numeric value.");
                }
                Console.WriteLine("Press any key to continue.");
                Console.ReadKey();
                Console.Clear();
            }
            
        }
        static void ReadFromLog()
        {
            try
            {
                Console.WriteLine(String.Format("Transaction history:\n{0}", GetContent(HttpMethod.Get, String.Format("{0}/api/transactions/all", Url))));
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("No transaction file found.");
            }
            catch(FormatException)
            {
                Console.WriteLine("No transaction found.");
            }
        }
        static decimal GetMoneyEarnedForTheCurrentPeriod()
        {
            TransactionInfo[] transactionInfos = JsonConvert.DeserializeObject<TransactionInfo[]>(GetContent(HttpMethod.Get, String.Format("{0}/api/transactions/last", Url)));
            decimal moneyEarned = 0;
            foreach (TransactionInfo transactionInfo in transactionInfos)
            {
                moneyEarned += transactionInfo.Sum;
            }
            return moneyEarned;
        }
        static void GetTransactionForTheCurrentPeriod()
        {
            TransactionInfo[] transactionInfos =  JsonConvert.DeserializeObject<TransactionInfo[]>(GetContent(HttpMethod.Get, String.Format("{0}/api/transactions/last", Url)));
            foreach (TransactionInfo transactionInfo in transactionInfos)
            {
                Console.WriteLine(transactionInfo.ToString());
            }
        }
        static int GetOccupiedPlaces()
        {
            return JsonConvert.DeserializeObject<int>(GetContent(HttpMethod.Get, String.Format("{0}/api/parking/capacity", Url))) - JsonConvert.DeserializeObject<int>(GetContent(HttpMethod.Get, String.Format("{0}/api/parking/freePlaces", Url)));
        }
        static void GetParkedVehicles()
        {
            ReadOnlyCollection<Vehicle> parkedVehicle = JsonConvert.DeserializeObject<ReadOnlyCollection<Vehicle>>(GetContent(HttpMethod.Get, String.Format("{0}/api/vehicles", Url)));
            foreach (Vehicle vehicle in parkedVehicle)
            {
                Console.WriteLine(vehicle.ToString());
            }
        }
        static void SetVehicleOnParking()
        {
            try
            {
                Console.WriteLine("Enter the license plate of the vehicle:");
                string licensePalte = Console.ReadLine();
                Console.WriteLine("Select the type of vehicle:");
                VehicleType vehicleType = new VehicleType();
                int vehicleTypeNumber = 0;
                Console.WriteLine("1. Passenger car");
                Console.WriteLine("2. Truck");
                Console.WriteLine("3. Bus");
                Console.WriteLine("4. Motorcycle");
                if (int.TryParse(Console.ReadLine(), out vehicleTypeNumber))
                {
                    switch (vehicleTypeNumber)
                    {
                        case 1:
                            vehicleType = VehicleType.PassengerCar;
                            break;
                        case 2:
                            vehicleType = VehicleType.Truck;
                            break;
                        case 3:
                            vehicleType = VehicleType.Bus;
                            break;
                        case 4:
                            vehicleType = VehicleType.Motorcycle;
                            break;
                        default:
                            Console.WriteLine("The entered value does not match any of the transport types.");
                            throw new ArgumentException();
                    }
                }
                else
                {
                    Console.WriteLine("The entered value is not a valid numeric value.");
                    throw new ArgumentException();
                }
                decimal balance = 0;
                Console.WriteLine("Enter vehicle balance: ");
                if (!decimal.TryParse(Console.ReadLine(), out balance))
                {
                    throw new ArgumentException();
                }
                Vehicle vehicle = new Vehicle(licensePalte, vehicleType, balance);
                GetContent(HttpMethod.Post, String.Format("{0}/api/vehicles/", Url), vehicle);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("One of the vehicle parameters was entered incorrectly.");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("There are no free parking places.");
            }
        }
        static void RemoveVehicleFromParking()
        {
            try
            {
                Console.WriteLine("Enter the vehicle license plate: ");
                GetContent(HttpMethod.Delete, String.Format("{0}/api/vehicles/{1}", Url, Console.ReadLine()));
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Incorrectly entered vehicle license plate.");
            }
            catch(InvalidOperationException)
            {
                Console.WriteLine("This vehicle has a debt. To pick up a vehicle, first clear it.");
            }
        }
        static void TopUpVehicleBalance()
        {
            try
            {
                Console.WriteLine("Enter the vehicle license plate: ");
                string licensePlate = Console.ReadLine();
                decimal sum = 0;
                Console.WriteLine("Enter the amount to replenish: ");
                if (!decimal.TryParse(Console.ReadLine(), out sum))
                {
                    throw new ArgumentException();
                }
                TransactionInfo transactionInfo = new TransactionInfo() { Sum = sum, VehicleId = licensePlate };
                GetContent(HttpMethod.Put, String.Format("{0}/api/transactions/topUpVehicle", Url), transactionInfo);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("One of the vehicle parameters was entered incorrectly.");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Required vehicle is not found.");
            }
        }
        static string GetContent(HttpMethod httpMethod, string uri, object sendedObject=null)
        {
            using (HttpClient client = new HttpClient())
            {
                var request = new HttpRequestMessage(httpMethod, uri);
                if (sendedObject != null) request.Content = new StringContent(JsonConvert.SerializeObject(sendedObject), Encoding.UTF8, "application/json");
                using (var response = client.Send(request, HttpCompletionOption.ResponseHeadersRead))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            return response.Content.ReadAsStringAsync().Result;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(String.Format("Problems while getting response. Message:{0}", ex.Message)); ;
                        }
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        if(httpMethod == HttpMethod.Delete)
                        {
                            throw new ArgumentException();
                        }
                        throw new InvalidOperationException();
                    }
                    else if (response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        throw new ArgumentException();
                    }
                }
            }
            return null;
        }
    }
}
    
