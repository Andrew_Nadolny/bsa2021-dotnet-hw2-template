﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private static Random random = new Random();
        [JsonRequired]
        public string Id { get; }
        [JsonRequired]
        [JsonConverter(typeof(VehicleTypeConverter))]
        public VehicleType VehicleType { get; }
        [JsonRequired]
        [JsonConverter(typeof(DecimalConverter))]
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            return String.Format("{0}-{1}-{2}", GenerateRandomTwoLetters(), GenerateRandomFourNumbers(), GenerateRandomTwoLetters())  ;
        }
        private static string GenerateRandomTwoLetters()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, 2)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        private static string GenerateRandomFourNumbers()
        {
            return random.Next(0,9999).ToString("D4");
        }
        public static bool LicensePlateValidaton(string id)
        {
            return Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
        }
        public override string ToString()
        {
            return String.Format("License plate: {0} - Type: {1} - Balance: {2}", Id, VehicleType.ToString("G"), Balance);
        }
    }
}