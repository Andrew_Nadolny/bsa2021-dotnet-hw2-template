﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; } 
        public LogService()
        {
            LogPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string Read()
        {
            if (File.Exists(LogPath))
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    return sr.ReadToEnd();
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(LogPath, true, System.Text.Encoding.Default))
            {
               if(logInfo != "")
                {
                    sw.WriteLine(logInfo);
                }
            }
        }
    }
}
