﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    class Parking
    {
        static Parking() { }
        private Parking() { }
        public static decimal Balance { get; set; } = Settings.StartingParkingBalance;
        public static List<Vehicle> ParkedVehicle { get; set; } = new List<Vehicle>();
        public static List<TransactionInfo> LastParkingTransaction { get; set; } = new List<TransactionInfo>();
        private static Parking source = null;
        public static Parking Source
        {
            get
            {
                if (source == null)
                    source = new Parking();

                return source;
            }
        }
    }
}