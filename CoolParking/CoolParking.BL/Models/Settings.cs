﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    static class Settings
    {
        public static decimal StartingParkingBalance { get; set; } = 0;
        public static int ParkingCapacity { get; set; } = 10;
        public static int PaymentChargePeriod { get; set; } = 5;
        public static int LoggingPeriod { get; set; } = 60;
        public static IReadOnlyDictionary<VehicleType, decimal> Tariffs { get;} = new Dictionary<VehicleType, decimal>()
            {
                { VehicleType.PassengerCar, 2 },
                { VehicleType.Truck, 5 },
                { VehicleType.Bus, 3.5M },
                { VehicleType.Motorcycle, 1 }
            };
        public static decimal PenaltyRate { get; set; } = 2.5M;
    }
}