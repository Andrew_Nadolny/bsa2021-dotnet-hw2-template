﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        [JsonRequired]
        [JsonConverter(typeof(DecimalConverter))]
        public decimal Sum { get; set; }
        public DateTime DateTime { get; set; }
        [JsonProperty("id")]
        [JsonRequired]
        public string VehicleId { get; set; }
        public override string ToString()
        {
            return String.Format("{0}: {1} money withdrawn from vehicle with Id=\'{2}\'", DateTime.ToString("dd/MM/yyyy hh:mm:ss tt"), Sum, VehicleId);
        }
    }
}
