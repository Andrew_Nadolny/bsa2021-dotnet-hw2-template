using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        IParkingService ParkingService;
        public ParkingController(IParkingService parkingService)
        {
            ParkingService = parkingService;
        }
        [HttpGet]
        [Route("/api/parking/balance")]
        public decimal GetBalance()
        {
            return ParkingService.GetBalance();
        }
        [HttpGet]
        [Route("/api/parking/capacity")]
        public int GetCapacity()
        {
            return ParkingService.GetCapacity();
        }
        [HttpGet]
        [Route("/api/parking/freePlaces")]
        public int GetFreePlaces()
        {
            return ParkingService.GetFreePlaces();
        }
        [HttpGet]
        [Route("/api/vehicles")]
        public string GetVehicles()
        {
            return JsonConvert.SerializeObject(ParkingService.GetVehicles());
        }
        [HttpGet]
        [Route("/api/vehicles/{id}")]
        public IActionResult GetVehicles(string id)
        {
            try
            {
                if (Vehicle.LicensePlateValidaton(id))
                {
                    return Ok(ParkingService.GetVehicles().First(x => x.Id == id.ToString()));
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            catch(ArgumentException)
            {
                return BadRequest();
            }
            catch(InvalidOperationException)
            {
                return NotFound();
            }
        }
        [HttpPost]
        [Route("/api/vehicles/")]
        public IActionResult PostVehicleAsync([FromBody] Vehicle vehicle)
        {
            try
            {
                ParkingService.AddVehicle(vehicle);
                return Created("/api/vehicles/" + vehicle.Id, vehicle);
            }
            catch (InvalidOperationException)
            {
                return BadRequest();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }
        [HttpDelete("{id}")]
        [Route("/api/vehicles/{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (Vehicle.LicensePlateValidaton(id))
            {
                Vehicle vehicle = ParkingService.GetVehicles().ToList().Find(x => x.Id == id);

                if (vehicle != null)
                {
                    if (vehicle.Balance > 0)
                    {
                        ParkingService.RemoveVehicle(id);
                        return NoContent();
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return NotFound();
                }

            }
            else
            {
                return BadRequest();
            }
        }
        [HttpGet]
        [Route("/api/transactions/last")]
        public string GetTransactionsLast()
        {
            return JsonConvert.SerializeObject(ParkingService.GetLastParkingTransactions());
        }
        [HttpGet]
        [Route("/api/transactions/all")]
        public IActionResult GetTransactionsAll()
        {
            try
            {
                return Ok(ParkingService.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }
        }
        [HttpPut]
        [Route("/api/transactions/topUpVehicle")]
        public IActionResult TopUpVehicle(TransactionInfo transactionInfo)
        {
            try
            {
                if(Vehicle.LicensePlateValidaton(transactionInfo.VehicleId) && transactionInfo.Sum > 0)
                {
                    Vehicle vehicle = ParkingService.GetVehicles().ToList().Find(x => x.Id == transactionInfo.VehicleId);
                    if (vehicle != null)
                    {
                        ParkingService.TopUpVehicle(transactionInfo.VehicleId, transactionInfo.Sum);
                        return Ok(vehicle);
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            catch(ArgumentException)
            {
                return NotFound();
            }
            catch(InvalidOperationException)
            {
                return BadRequest();
            }
        }
    }
}
