﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimeService : ITimerService
    {
        private Timer timer;
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Start()
        {
            timer = new Timer(Interval);
            timer.Elapsed += Elapsed;
            timer?.Start();
        }
        public void Stop()
        {
            timer?.Stop();
        }
        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
