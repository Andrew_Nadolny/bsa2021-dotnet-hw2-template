// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        ITimerService WithdrawTimer;
        ITimerService LogTimer;
        ILogService LogService;
        public ParkingService(): this(new TimeService(), new TimeService(), new LogService()) {}
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WithdrawTimer = withdrawTimer;
            WithdrawTimer.Interval = Settings.PaymentChargePeriod * 1000;
            WithdrawTimer.Elapsed += Withdraw;
            WithdrawTimer.Start();
            LogTimer = logTimer;
            LogTimer.Interval = Settings.LoggingPeriod * 1000;
            LogTimer.Elapsed += Logging;
            LogTimer.Start();
            LogService = logService;
        }
        private void Logging(object sender, ElapsedEventArgs e)
        {
            string transactionInfoToSting = "";
            foreach (TransactionInfo transactionInfo in Parking.LastParkingTransaction)
            {
                transactionInfoToSting += transactionInfo.ToString()+"\n";
            }
            transactionInfoToSting.TrimEnd();
            LogService.Write(transactionInfoToSting.TrimEnd('\n'));
            Parking.LastParkingTransaction.Clear();
        }
        private void Withdraw(object sender, ElapsedEventArgs e)
        {
            foreach(Vehicle vehicle in Parking.ParkedVehicle)
            {
                decimal withdrawAmount = 0;
                if (vehicle.Balance >= Settings.Tariffs[vehicle.VehicleType])
                {
                    Parking.Balance += withdrawAmount = Settings.Tariffs[vehicle.VehicleType];
                }
                else if (vehicle.Balance > 0 && vehicle.Balance < Settings.Tariffs[vehicle.VehicleType])
                {
                    Parking.Balance += withdrawAmount = vehicle.Balance + (Settings.Tariffs[vehicle.VehicleType] - vehicle.Balance) * Settings.PenaltyRate;
                }
                else
                {
                    Parking.Balance += withdrawAmount = Settings.Tariffs[vehicle.VehicleType] * Settings.PenaltyRate;
                }
                vehicle.Balance -= withdrawAmount;
                Parking.LastParkingTransaction.Add(new TransactionInfo() { DateTime = DateTime.Now, Sum = withdrawAmount, VehicleId = vehicle.Id });
            }
        }
        public decimal GetBalance()
        {
            return Parking.Balance;
        }
        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }
        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - Parking.ParkedVehicle.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.ParkedVehicle.AsReadOnly();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if(Parking.ParkedVehicle.Count < Settings.ParkingCapacity)
            {
                if(Vehicle.LicensePlateValidaton(vehicle.Id) && Enum.IsDefined(vehicle.VehicleType) && vehicle.Balance > 0 && Parking.ParkedVehicle.Find(x => x.Id == vehicle.Id) == null)
                {
                    Parking.ParkedVehicle.Add(vehicle);
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
        public void RemoveVehicle(string vehicleId)
        {
            if (Vehicle.LicensePlateValidaton(vehicleId))
            {
                Vehicle vehicle = Parking.ParkedVehicle.Find(x => x.Id == vehicleId);
                if(vehicle != null)
                {
                    if (vehicle.Balance > 0)
                    {
                        Parking.ParkedVehicle.Remove(vehicle);
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (Vehicle.LicensePlateValidaton(vehicleId) && sum > 0)
            {
                Vehicle vehicle = Parking.ParkedVehicle.Find(x => x.Id == vehicleId);
                if(vehicle != null)
                {
                    vehicle.Balance = vehicle.Balance + sum;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Parking.LastParkingTransaction.ToArray();
        }
        public string ReadFromLog()
        {
            return LogService.Read();
        }
        public void Dispose()
        {
            WithdrawTimer?.Dispose();
            LogTimer?.Dispose();
            Parking.Balance = Settings.StartingParkingBalance;
            Parking.ParkedVehicle.Clear();
            Parking.LastParkingTransaction.Clear();
        }
    }
}